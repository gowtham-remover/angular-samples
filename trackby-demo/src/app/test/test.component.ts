import { Component } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent {

  employees: any[];
  constructor() {
    this.employees = [
      { id: '100', name: 'Alastair Cook' },
      { id: '101', name: 'Mark Wood' },
      { id: '102', name: 'Joe Root' },
      { id: '103', name: 'Jack Ball' }
    ];
  }


  getEmployeeListTwo() {
    this.employees = [
      { id: '100', name: 'Alastair Cook' },
      { id: '101', name: 'Mark Wood' },
      { id: '102', name: 'Je Root' },
      { id: '103', name: 'Jack Ball' },
      { id: '104', name: 'Ryan Bird' }
    ];
  }

  track(index: number, employee: any) {
    return employee.id;
  }

}
