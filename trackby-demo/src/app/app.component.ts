import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  employees: any[];
  constructor() {
    this.employees = [
      { id: '100', name: 'Alastair Cook' },
      { id: '101', name: 'Mark Wood' },
      { id: '102', name: 'Joe Root' },
      { id: '103', name: 'Jack Ball' }
    ];
  }
  getEmployeeListOne() {
    this.employees = [
      { id: '100', name: 'Alastair Cook' },
      { id: '101', name: 'Mark Wood' },
      { id: '102', name: 'Joe Root' },
      { id: '103', name: 'Jack Ball' },
      { id: '104', name: 'Remover' }
    ];
  }


  track(index: number, employee: any) {
    return employee.id;
  }

}
