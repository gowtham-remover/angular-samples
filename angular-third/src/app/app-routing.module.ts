import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CricketComponent } from './cricket/cricket.component';
import { FootballComponent } from './football/football.component';


const routes: Routes = [
   {path: 'cricket', component: CricketComponent},
   {path: 'football', component: FootballComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
