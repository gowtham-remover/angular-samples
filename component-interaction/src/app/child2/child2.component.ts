import { Component, OnInit } from '@angular/core';
import { CommunicationService } from '../communication.service';

@Component({
  selector: 'app-child-two',
  templateUrl: './child2.component.html',
  styleUrls: ['./child2.component.css']
})
export class Child2Component {

  constructor(private communicationService: CommunicationService) { }

  data: any = "Data Received from Child Component";
  dataFromC1: any;
  dataArray: any;
  id: string;
  name: string;

  ngOnInit() {
    this.communicationService.getCall().subscribe(data => {
      this.dataFromC1 = data;
    });
    this.communicationService.getData().subscribe(data => {
      this.id = data['id'];
      this.name = data['name'];
    })
  }

  passData() {
    this.communicationService.setCall(this.data);
  }

}
