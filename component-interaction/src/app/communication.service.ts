import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  private subject = new Subject();
  public dataUrl = 'assets/data.json';

  constructor(private http: HttpClient) { }

  setCall(message: any) {
    this.subject.next(message);
  }

  getCall(): Observable<any> {
    return this.subject.asObservable();
  }

  getData(): Observable<any> {
    return this.http.get(this.dataUrl);
  }
}
export interface Data {
  id: string;
  name: string;
}