import { Component, OnInit } from '@angular/core';
import { CommunicationService, Data } from '../communication.service';

@Component({
  selector: 'app-child-three',
  templateUrl: './child3.component.html',
  styleUrls: ['./child3.component.css']
})
export class Child3Component implements OnInit {

  public data: any;
  public id: string;
  public name: string;

  constructor(private communicationService: CommunicationService) { }

  temp: any;

  ngOnInit() {
  }
  processData() {
    this.communicationService.getCall().subscribe((data: Data) => {
      this.id = data['id'];
      this.name = data['name'];
      this.temp = data;
    });

    this.communicationService.getData().subscribe((data: Data) => {
      this.id = data['id'];
      this.name = data['name'];
    });
  }
}
