import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Input } from '@angular/core';
import { CommunicationService } from '../communication.service';

@Component({
  selector: 'app-child-one',
  templateUrl: './child1.component.html',
  styleUrls: ['./child1.component.css']
})
export class Child1Component implements OnInit {

  @Output()
  emitter: EventEmitter<string> = new EventEmitter<string>();

  @Input()
  parentName: string;

  dataFromC2: any;
  data2: any = 'Data from C2 Received';
  childName: string = 'Child';
  constructor(private communicationService: CommunicationService) { }

  ngOnInit() {
    this.communicationService.getCall().subscribe(data => {
      this.dataFromC2 = data;
    });
  }

  onChange() {
    this.emitter.emit(this.childName);
  }

  transferData() {
    this.communicationService.setCall(this.data2);
  }
}
