import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor() { }

  getEmployee() {
    return [ 'Brendon de Gow', 'Remover', 'Cook', 'ABD', 'MSD', 'Adam Gilly'];
  }
}
