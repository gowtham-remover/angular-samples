import { Component,OnInit } from '@angular/core';
import { EmployeeService } from './employee.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  name = 'Gowtham';
  isGood: boolean = false;
  public employee = [];

  constructor(private empService : EmployeeService) {
  }

  ngOnInit() {
    this.employee = this.empService.getEmployee();
  }
  save() {
    console.log("Hi Gowtham i am button");
  }
}
