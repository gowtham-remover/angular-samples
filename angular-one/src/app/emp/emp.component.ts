import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-emp',
  templateUrl: './emp.component.html',
  styleUrls: ['./emp.component.css']
})
export class EmpComponent implements OnInit {

  public employeeDetail = [];

  constructor(private empService: EmployeeService) {

  }
  ngOnInit() {
    this.employeeDetail = this.empService.getEmployee();
  }

}
