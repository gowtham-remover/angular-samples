import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
   players: any[];

   constructor () {
  this.players = [
    {id: '100', name: 'steyn'},
    {id: '101', name: 'cook'},
    {id: '102', name: 'gambhir'},
    {id: '103', name: 'wood'}
  ];
}
}
