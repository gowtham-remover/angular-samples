import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonService } from '../common.service';

import { Router } from "@angular/router";

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

  public filterForm;
  public countries: any;
  public categories: any = ['All Category', 'Business', 'Entertainment', 'General', 'Health', 'Science', 'Sports', 'Technology'];
  public sources: any = ['All Sources', 'abc-news', 'bbc-news', 'bbc-sports', 'business-insider', 'cbc-news', 'cnbc', 'cnn', 'daily-mail', 'espn', 'fortune', 'google-news'];
  public languages: any = ['en', 'ab', 'por', 'tur', 'dothraki'];

  public count = 0;
  public flag = 0;
  public ampersandRequired = 0;
  public api: string = 'https://newsapi.org/v2/';

  constructor(private commonService: CommonService, private router: Router) {
    this.filterForm = new FormGroup({
      filterType: new FormControl("headlines"),
      HeadlineFilterType: new FormControl("other"),
      country: new FormControl(true),
      category: new FormControl(),
      countrySelect: new FormControl("us"),
      categorySelect: new FormControl("All Category"),
      headlineQuery: new FormControl(),
      everythingQuery: new FormControl(null, Validators.required),
      sourceSelect: new FormControl('All Sources'),
      languageSelect: new FormControl('en'),
      fromDate: new FormControl(),
      toDate: new FormControl(),
      language: new FormControl()
    });

  }
  ngOnInit() {
    this.commonService.getNews().subscribe(data => {
      this.countries = data;
    });
  }
  onSubmit() {

    this.commonService.setApi(this.validate(this.filterForm.value));
    this.router.navigate(['news']);
  }

  validate(formValue: any) {
    if (formValue.filterType == "headlines") {
      this.api += 'top-headlines?';
      if (formValue.HeadlineFilterType == "other") {
        if (formValue.country == true) {
          this.api += 'country=' + formValue.countrySelect;
        } else {
          this.api += 'country=us';
        }
        if (formValue.category == true) {
          if (formValue.categorySelect == "All Category") {
          } else {
            this.api += '&category=' + formValue.categorySelect;
          }
        }
        else { }
        if (formValue.headlineQuery == null) {

        } else {
          console.log("Query " + formValue.headlineQuery);
          this.api += '&q=' + formValue.headlineQuery;
        }
      } else {
        if (formValue.sourceSelect == "All Sources") {
        } else {
          this.count++;
          this.api += 'sources=' + formValue.sourceSelect;
          this.ampersandRequired++;
        }
        if (formValue.headlineQuery == null || formValue.headlineQuery == "") {
        } else {
          this.flag++;
          if (this.ampersandRequired == 0) {
            this.api += 'q=' + formValue.headlineQuery;
          } else {
            this.api += '&q=' + formValue.headlineQuery;
          }
        }
        if (this.count == 0 && this.flag == 0) {
          this.api += 'country=us';
        }
      }
      this.api += '&apiKey=38346e2a5f7a49dab26a3f795ab862e7';
    } else {
      this.api += 'everything?q=' + formValue.everythingQuery;
      if (formValue.language == true && formValue.languageSelect !== "en") {
        this.api += '&language=' + formValue.languageSelect;
      }
      if (formValue.fromDate != null) {
        this.api += '&from=' + formValue.fromDate;
      }
      if (formValue.toDate != null) {
        this.api += '&to=' + formValue.toDate;
      }
      this.api += '&apiKey=38346e2a5f7a49dab26a3f795ab862e7';
    }
    return this.api;
  }
}
