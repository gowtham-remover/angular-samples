import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { AppComponent } from './app.component';
import { NewsComponent } from './news/news.component';
import { FilterComponent } from './filter/filter.component';
import { AboutComponent } from './about/about.component';
import { NewsviewComponent } from './news/newsview/newsview.component';

const routes: Routes = [

  {
    path: 'news',
    component: NewsComponent
  },
  {
    path: 'filter',
    component: FilterComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'readnews/:newsId',
    component: NewsviewComponent
  },
  {
    path: '',
    redirectTo: '/news',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: PagenotfoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
