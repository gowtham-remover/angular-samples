import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { NewsComponent } from './news/news.component';
import { FilterComponent } from './filter/filter.component';
import { AboutComponent } from './about/about.component';
import { MatTabsModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatRadioModule,
  MatCheckboxModule,
  MatSelectModule,
  MatFormFieldModule,
  MatInputModule
} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { FormsModule } from '@angular/forms';
import { NewsviewComponent } from './news/newsview/newsview.component';
import { CommonService } from './common.service';
import { HttpErrorInterceptor } from './http-error-interceptor.service';
 
@NgModule({
  declarations: [
    AppComponent,
    PagenotfoundComponent,
    NewsComponent,
    FilterComponent,
    AboutComponent,
    NewsviewComponent
  ],
  imports: [
    BrowserModule,
    MatTabsModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    MatRadioModule,
    MatCheckboxModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    RouterModule,
    ScrollingModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [CommonService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
