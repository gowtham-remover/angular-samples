import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { NewsComponent, Articles } from './news/news.component';
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CommonService implements OnInit {

  public url: string;
  public apiData: string = "./assets/api.json";
  public service = new BehaviorSubject('');
  public selectedNews;
  public publishTitle;
  constructor(private http: HttpClient) { }
  public api = 'https://newsapi.org/v2/top-headlines?country=us&apiKey=38346e2a5f7a49dab26a3f795ab862e7';

  ngOnInit() {
  }

  getDefaultNews() {
     // http client error handling
    // return this.http.get<Articles>(this.api).pipe(retry(1),catchError(this.handleError));
    // normal call to the api
    return this.http.get<Articles>(this.api);
  }

  getNews() {
    return this.http.get<Articles>(this.apiData);
  }

  setApi(val) {
    this.api = val;
  }

  setSelectedNews(value) {
    this.selectedNews = value;
  }

  getSelectedNews() {
    return this.selectedNews;
  }

  setPublishTitles(titleList) {
    this.publishTitle = titleList;
  }

  getPublishTitles() {
    return this.publishTitle;
  }
  // http client error handling
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
