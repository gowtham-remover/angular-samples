import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../common.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-newsview',
  templateUrl: './newsview.component.html',
  styleUrls: ['./newsview.component.css']
})
export class NewsviewComponent implements OnInit {

  public news?: any;
  public today: number = Date.now();
  public newsTitle;
  public tempArray: string[] = [];

  constructor(private commonService: CommonService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params=> {
      this.newsTitle = params['newsId'];
      console.log(this.newsTitle);
    }); 
    this.tempArray = this.commonService.getPublishTitles();
    console.log(this.tempArray.includes(this.newsTitle));                  
    this.news = this.commonService.getSelectedNews();
  }
}
