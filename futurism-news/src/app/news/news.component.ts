import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';

export interface Articles {
  status?: string;
  totalResults?: number;
  articles?: Article[];
}

export interface Article {
  author?: string;
  content?: string;
  description?: string;
  publishedAt?: string;
  source?: Source;
  title?: string;
  url?: string;
  urlToImage?: string;
  urlTitle?: string
}

export interface Source {
  id: string;
  name: string;
}

// duplicate news to check the structure of api hit
const dummy: Articles = {
  status: "okk",
  totalResults: 33,
  articles: <Article[]>[
    {
      author: "remover",
      content: "The remover is left arm fast from phallaborwa end",
      description: "He another name is Mr. Fast",
      publishedAt: "20/02/2019",
      source: {
        id: "ID001",
        name: "cricbuzz.com"
      },
      title: "Mr.Speed Goes Bang",
      url: "http://google.com",
      urlToImage: "http://google.com"
    }
  ]
};

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})

export class NewsComponent implements OnInit {

  public type: string = 'top-headlines';
  public country: string = 'us';
  public news?: Articles;
  public today: number = Date.now();
  public initialApi: string;
  public initialTitle;
  public titleArray: any = [];
  constructor(private commonService: CommonService) {
  }

  ngOnInit() {
      this.commonService.getDefaultNews().subscribe((data: Articles) => {
        this.news = data;
        console.log(this.news);
        var i = 0;
        for (let article of this.news.articles) {
          // var result = article.title.substring(0, article.title.indexOf(" -"))
          var arrResult = article.title.split(" ");
          this.news.articles[i].urlTitle = arrResult.join("-");
          this.titleArray.push(this.news.articles[i].urlTitle); 
          i++;
        }
      });
      this.commonService.setPublishTitles(this.titleArray);
  }

  onSelectNews(val) {
    this.commonService.setSelectedNews(val);
  }
}
